		//Letra A
		/* Não pode ser compilado, pois o x começa em 1
		 * entra num loop que verifica se é menor que 10,
		 * depois tem uma condição se caso for maior que 3 ele imprima
		 * só que ele nunca entra na condição, e fica um loop infinito,
		 * porque não há incremento do X
		 * 
		 *  Para consertar mantendo o programa do jeito que está, eu incrementaria
		 *  o x cada vez que ele passasse no loop*/
		
	
		//Letra B
		
		/* Não pode ser compilado pela ausência da "class", portanto minha
		 * alteração para fazer funcionar seria simplesmente adiciona-la a uma 
		 * class e o programa funcionaria
		 */
		// Pode ser compilado
		
		//Letra C
		
		/* Não pode ser compilado pela falta do main, para compilar é necessário
		 * adicionar o main.
		 */
	

