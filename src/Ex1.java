import java.util.Scanner;
public class Ex1 {
	
	public static void main (String []args) {
		
		//Letra A
		for(int i=150;i<=300;i++) {
		System.out.println(i);
		}
		
		//Letra B
		int sum = 0;
		
		for(int i=1;i<=1000;i++) {
			sum = sum+i;
		}
		System.out.println("A soma é:" + sum);
		
		//Letra C
		for(int i=0;i<=100;i++) {
			if(i%3 == 0) {
				System.out.println(i);
			}
		}
		
		//Letra D e E
		long fatorial = 1;

	    for (int i = 1;i < 11 ; i ++ ) {
	      fatorial *= i;

	      System.out.println("Fatorial de " +i+"=" +fatorial);
	    }
	    
	    //Letra F
	    Scanner f = new Scanner(System.in);
	    
	    System.out.println("Digite a quantidade de termos");
	    int qtd = f.nextInt();
	    int n1 = 1;
	    int n2 = 1;
	    System.out.print("1 ");
	    System.out.print("1 ");
	    qtd = qtd - 2;
	    while (qtd > 0) {
	    	if ((n1+n2)>100)
	        	break;
	    	System.out.print((n1+n2) + " ");
	        int n3 = n1+n2;
	        n1 = n2;
	        n2 = n3;
	        qtd--;
	    }
	    
	    //Letra G
	    
	    Scanner keyboard = new Scanner(System.in);
	    
	    System.out.println("Digite o número de X:");
	    double x = keyboard.nextDouble();
	    
	    while(true) {
	    	if(x%2 == 0) {
	    		x = x/2;
	    	}
	    	else
	    		x = (x*3)+1;
	    	System.out.println("O novo valor de x é" + x);
	    	
	    	if(x==1)
	    		break;
	    }
	    
	    keyboard.close();
	    
	}
}

