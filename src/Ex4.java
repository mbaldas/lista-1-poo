/* No item A, o programa não pode ser compilado,
   pois na classe TestDrive, ele não está instanciando uma nova TapeDeck,
   está tentando chamar direto numa variavel t qualquer, e não irá funcionar
   para funcionar ele deveria instanciar, como por exemplo:
   
   TapeDeck t = new TapeDeck();
*/

/* No item B, o programa também não pode ser compilado, o erro agora é outro,
 * ele instancia um objeto da classe DVD, porém chama um metódo que nao está 
 * declarado que é o playDVD, logo ele não consegue compilar
 */
